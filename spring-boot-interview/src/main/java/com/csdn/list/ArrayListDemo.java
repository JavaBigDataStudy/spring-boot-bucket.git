package com.csdn.list;

import lombok.extern.slf4j.Slf4j;

/**
 * @Description: ArrayList基础API操作 -【快速随机访问】优势
 * @Author Cheri
 * @Date 2019/8/2 - 1:01
 * @Version V1.0
 **/
@Slf4j
public class ArrayListDemo {

    /*
     *  启动类
     * @decription
     * @author dukang
     * @date 2019/8/2 13:41
     * @param [args]
     * @return void
     */
    public static void main(String[] args) {
        log.info("程序开始执行...");

        log.info("程序执行结束...");

    }

}


