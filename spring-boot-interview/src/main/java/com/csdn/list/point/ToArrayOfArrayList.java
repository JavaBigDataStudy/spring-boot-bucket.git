package com.csdn.list.point;

import java.util.ArrayList;

/**
 * @Description: TODO
 * @Author Cheri
 * @Date 2019/8/2 - 19:21
 * @Version V1.0
 **/
public class ToArrayOfArrayList {

    // toArray(T[] contents)调用方式一
    public static Integer[] vectorToArray1(ArrayList<Integer> v) {
        Integer[] newText = new Integer[v.size()];
        v.toArray(newText);
        return newText;
    }

    // toArray(T[] contents)调用方式二。最常用！
    public static Integer[] vectorToArray2(ArrayList<Integer> v) {
        Integer[] newText = (Integer[])v.toArray(new Integer[0]);
        return newText;
    }

    // toArray(T[] contents)调用方式三
    public static Integer[] vectorToArray3(ArrayList<Integer> v) {
        Integer[] newText = new Integer[v.size()];
        Integer[] newStrings = (Integer[])v.toArray(newText);
        return newStrings;
    }

    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
        ArrayList arrayLsit = new ArrayList<>();
        arrayLsit.add(1);
        arrayLsit.add(2);
        arrayLsit.add(3);
//        Integer[] integers = vectorToArray1(arrayLsit);
//        Integer[] integers = vectorToArray2(arrayLsit);
        Integer[] integers = vectorToArray3(arrayLsit);
        for (Integer integer : integers){
            System.out.println(integer);
        }

        Integer[] integers1 = new Integer[11];

        Integer[] integers12 = (Integer[]) arrayLsit.toArray(integers1);
        System.out.println(integers1==integers12);

//        for (Integer integer: integers1) {
//            System.out.println(integer);
//        }
    }
}
