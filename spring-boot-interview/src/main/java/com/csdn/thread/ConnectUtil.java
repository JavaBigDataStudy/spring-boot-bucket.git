package com.csdn.thread;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @Description: TODO
 * @ClassName ConnectUtil
 * @Author Cheri
 * @Date 2019/7/31 - 11:32
 * @Version V1.0
 **/
public class ConnectUtil {
    private static String driveClassName = "com.mysql.jdbc.Driver";
    private static String url = "jdbc:mysql://localhost:3307/test?serverTimezone=GMT&useUnicode=true&characterEncoding=utf8";

    private static String user = "root";
    private static String password = "root";

    public static Connection Connect(){
        Connection conn = null;

        //load driver
        try {
            Class.forName(driveClassName);
        } catch (ClassNotFoundException  e) {
            System.out.println("load driver failed!");
            e.printStackTrace();
        }

        //connect db
        try {
            conn = DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            System.out.println("connect failed!");
            e.printStackTrace();
        }

        return conn;
    }

    public static void main(String[] args) {
        System.out.println(ConnectUtil.Connect());
    }
}
