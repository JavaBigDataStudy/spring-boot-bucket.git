package com.csdn.thread.others;

/**
 * @Description: Random类学习
 * @ClassName RandomTest
 * @Author Cheri
 * @Date 2019/7/30 - 18:41
 * @Version V1.0
 **/
import org.junit.Test;
import java.util.Random;

public class RandomTest {

    /**
     * 测试Random类中的简单方法
     */
    @Test
    public void test02() {
        Random random = new Random();
        System.out.println("nextInt()：" + random.nextInt());   //随机生成一个整数，这个整数的范围就是int类型的范围-2^31~2^31-1
        System.out.println("nextLong()：" + random.nextLong());      //随机生成long类型范围的整数
        System.out.println("nextFloat()：" + random.nextFloat());    //随机生成[0, 1.0)区间的小数
        System.out.println("nextDouble()：" + random.nextDouble());  //随机生成[0, 1.0)区间的小数

        byte[] byteArr = new byte[10];
        random.nextBytes(byteArr);  //随机生成byte，并存放在定义的数组中，生成的个数等于定义的数组的个数
        for (int i = 0; i < byteArr.length; i++) {
            System.out.println(byteArr[i]);
        }

        /**
         * random.nextInt(n)
         * 随机生成一个正整数，整数范围[0,n)
         * 如果想生成其他范围的数据，可以在此基础上进行加减
         *
         * 例如：
         * 1. 想生成范围在[0,n]的整数
         *      random.nextInt(n+1)
         * 2. 想生成范围在[m,n]的整数, n > m
         *      random.nextInt(n-m+1) + m
         * 3. 想生成范围在(m,n)的整数
         *      random.nextInt(n-m+1) + m + 1
         * ...... 主要是依靠简单的加减法
         * 4. 想生成范围在(m,n]的整数
         * 5. 想生成范围在[m,n)的整数
         */
        System.out.println("nextInt(10)：" + random.nextInt(10)); //随机生成一个整数，整数范围[0,10)
        for (int i = 0; i < 20; i++) {
            //[3,15)
            //这里有坑，需要注意，如果前面用了+号，应该要把计算结果整体用括号括起来，不然它会把+号解释为字符串拼接
            System.out.println("我生成了一个[3,15)区间的数，它是：" + (random.nextInt(12) + 3));
        }

        System.out.println("++++++++调用方法randomInt（10,20）;则会生成一个[10,20]集合内的任意整数+++++++++++++>");
        for (int i = 0; i< 20; i++){
//            System.out.println(randomInt(2,7));
            System.out.println(random.nextInt());
        }


    }

    /*
     * 生成[min, max]之间的随机整数
     * @param min 最小整数
     * @param max 最大整数
     */
    private static int randomInt(int min, int max){
        return new Random().nextInt(max)%(max-min+1) + min;
    }

    /**
     * 测试Random类中 JDK1.8提供的新方法
     * JDK1.8新增了Stream的概念
     * 在Random中，为double, int, long类型分别增加了对应的生成随机数的方法
     * 鉴于每种数据类型方法原理是一样的，所以，这里以int类型举例说明用法
     */
    @Test
    public void test03() {
        Random random = new Random();
        random.ints();  //生成无限个int类型范围内的数据，因为是无限个，这里就不打印了，会卡死的......
        random.ints(10, 100);   //生成无限个[10,100)范围内的数据

        /**
         * 这里的toArray 是Stream里提供的方法
         */
        int[] arr = random.ints(10).toArray();  //生成10个int范围类的个数。
        System.out.println(arr.length);
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }


        //生成5个在[10,100)范围内的整数
        random.ints(5, 10, 100).forEach(System.out :: println); //这句话和下面三句话功能相同
        //forEach等价于：
        arr = random.ints(5, 10, 100).toArray();
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }

        /**
         * 对于
         *      random.ints();
         *      random.ints(ori, des);
         * 两个生成无限个随机数的方法，我们可以利用Stream里的terminal操作，来截断无限这个操作
         */
        //limit表示限制只要10个，等价于random.ints(10)
        random.ints().limit(10).forEach(System.out :: println);

        //等价于random.ints(5, 10, 100)
        random.ints(10, 100).limit(5).forEach(System.out :: println);
    }

}
