package com.csdn.thread.create.callable.demo;

import java.util.concurrent.*;

/**
 * @Description: TODO
 * @ClassName CallableAndFutureDemo
 * @Author Cheri
 * @Date 2019/7/30 - 1:55
 * @Version V1.0
 **/
public class CallableAndFutureDemo {

    public static void main(String[] args) {
        ExecutorService executorService = Executors.newCachedThreadPool();
        Task task = new Task();
        Future<Integer> future = executorService.submit(task);
        executorService.shutdown();

        System.out.println("主线程在执行任务...");
        try {
            System.out.println("主线程耗时开始");
            Thread.sleep(2000);
            System.out.println("主线程耗时结束");
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }

        try {
            System.out.println("task运行结果:" + future.get());
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        } catch (ExecutionException ex) {
            ex.printStackTrace();
        }
        System.out.println("所有任务执行完毕");
    }
}
//
class Task implements Callable<Integer> {
    @Override
    public Integer call() throws Exception {
        System.out.println("子线程在执行任务...");
        //模拟任务耗时
        Thread.sleep(5000);
        return 1000;
    }

}
