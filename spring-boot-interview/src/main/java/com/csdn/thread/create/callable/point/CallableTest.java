package com.csdn.thread.create.callable.point;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * @Description: TODO
 * @ClassName CallableTest
 * @Author Cheri
 * @Date 2019/7/30 - 1:14
 * @Version V1.0
 **/

public class CallableTest {
    public static void main(String[] args) {
        ExecutorService exec=Executors.newCachedThreadPool();
//        ExecutorService fixedThreadPool = Executors.newFixedThreadPool(3);
        List<Future<String>> results=new ArrayList<Future<String>>();

        for(int i=0;i<5;i++) {
            results.add(exec.submit(new TaskWithResult(i)));
        }

        for(Future<String> fs :results) {
            try {
                System.out.println(fs.get());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
    }
}

class TaskWithResult implements Callable<String> {
    private int id;
    public TaskWithResult(int id) {
        this.id=id;
    }

    @Override
    public String call() throws Exception {
        return "result of TaskWithResult "+id;
    }
}