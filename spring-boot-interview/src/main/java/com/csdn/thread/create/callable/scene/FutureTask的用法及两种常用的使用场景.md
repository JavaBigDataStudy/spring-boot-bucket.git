## [FutureTask的用法及两种常用的使用场景](https://1028826685.iteye.com/blog/2307620)

### 简单介绍

> - [ ] **FutureTask可用于异步获取执行结果或取消执行任务的场景**。通过传入Runnable或者Callable的任务给FutureTask，直接调用其run方法或者放入线程池执行，之后可以在外部通过FutureTask的get方法异步获取执行结果，因此，FutureTask非常适合用于耗时的计算，主线程可以在完成自己的任务后，再去获取结果。另外，FutureTask还可以确保即使调用了多次run方法，它都只会执行一次Runnable或者Callable任务，或者通过cancel取消FutureTask的执行等。 
> - [ ] FutureTask包装器是一种非常便利的机制，同时实现了Future和Runnable接口 

### 使用场景演示

1. **FutureTask执行多任务计算的使用场景**

   ​	利用FutureTask和ExecutorService，可以用多线程的方式提交计算任务，主线程继续执行其他任务，当主线程需要子线程的计算结果时，再异步的获取子线程的执行结果。 

   ```java
   package com.csdn.thread.create.callable.scene.cases01;
   
   import java.util.ArrayList;
   import java.util.List;
   import java.util.concurrent.*;
   
   /**
    * @Description: FutureTask执行多任务计算的使用场景
    * @ClassName FutureTaskForMultiCompute
    * @Author Cheri
    * @Date 2019/7/31 - 10:21
    * @Version V1.0
    **/
   public class FutureTaskForMultiCompute {
   
       public static void main(String[] args) {
   
           FutureTaskForMultiCompute inst=new FutureTaskForMultiCompute();
           // 创建任务集合
           List<FutureTask<Integer>> taskList = new ArrayList<FutureTask<Integer>>();
           // 创建线程池
           ExecutorService exec = Executors.newFixedThreadPool(5);
           for (int i = 0; i < 10; i++) {
               // 传入Callable对象创建FutureTask对象
               FutureTask<Integer> ft = new FutureTask<Integer>(inst.new ComputeTask(i, ""+i));
               //添加到任务集合中
               taskList.add(ft);
               // 提交给线程池执行任务，
               exec.submit(ft);
           }
           //也可以通过exec.invokeAll(taskList)一次性提交所有任务;
   
           System.out.println("所有计算任务提交完毕, 主线程接着干其他事情！");
   
           // 开始统计各计算线程计算结果
           Integer totalResult = 0;
           for (FutureTask<Integer> ft : taskList) {
               try {
                   //FutureTask的get方法会自动阻塞,直到获取计算结果为止
                   totalResult = totalResult + ft.get();
               } catch (InterruptedException e) {
                   e.printStackTrace();
               } catch (ExecutionException e) {
                   e.printStackTrace();
               }
           }
   
           // 关闭线程池
           exec.shutdown();
           System.out.println("多任务计算后的总结果是:" + totalResult);
   
       }
   
       //计算任务
       private class ComputeTask implements Callable<Integer> {
   
           private Integer result = 0;
           private String taskName = "";
   
           public ComputeTask(Integer iniResult, String taskName){
               result = iniResult;
               this.taskName = taskName;
               System.out.println("生成子线程计算任务: "+taskName+" result"+result);
           }
   
           public String getTaskName(){
               return this.taskName;
           }
   
           //任务处理方法
           @Override
           public Integer call() throws Exception {
   
               for (int i = 0; i < 10; i++) {
                   result += i;
               }
               // 休眠5秒钟，观察主线程行为，预期的结果是主线程会继续执行，到要取得FutureTask的结果是等待直至完成。
               Thread.sleep(5000);
               System.out.println("子线程计算任务: "+taskName+" 执行完成!"+" result:"+result);
               return result;
           }
       }
   
   }
   ```

   

2. **FutureTask在高并发环境下确保任务只执行一次** 

   FutureTask还可以确保即使调用了多次run方法,它都只会执行一次Runnable或者Callable任务,或者通过cancel取消FutureTask的执行等 

   ​	在很多高并发的环境下，往往我们只需要某些任务只执行一次。这种使用情景FutureTask的特性恰能胜任。举一个例子，假设有一个带key的连接池，当key存在时，即直接返回key对应的对象；当key不存在时，则创建连接。对于这样的应用场景，通常采用的方法为使用一个Map对象来存储key和连接池之间的对应关系 

   ```java
   public class FutureTaskOnceOnConcurrency {
   
       private Map<String, Connection> connectionPool = new HashMap<String, Connection>();
       private ReentrantLock lock = new ReentrantLock();
   
       public Connection getConnection(String key){
           try{
               lock.lock();
               if(connectionPool.containsKey(key)){
                   return connectionPool.get(key);
               }
               else{
                   //创建 Connection
                   Connection conn = createConnection();
                   connectionPool.put(key, conn);
                   return conn;
               }
           }
           finally{
               lock.unlock();
           }
       }
   
       //创建Connection
       private Connection createConnection(){
           return null;
       }
   }
   ```

   通过加锁确保高并发环境下的线程安全，也确保了connection只创建一次，然而确牺牲了性能。改用ConcurrentHash的情况下，几乎可以避免加锁的操作，性能大大提高，但是在高并发的 情况下有可能出现Connection被创建多次的现象 。这时最需要解决的问题就是当key不存在时，创建Connection的动作能放在connectionPool之后执行，这正是FutureTask发挥作用的时机，基于ConcurrentHashMap和FutureTask的改造代码如下： 

   ```java
   public class NoLockSafeFuture {
   
       private ConcurrentHashMap<String, FutureTask<Connection>> connectionPool = new ConcurrentHashMap<String, FutureTask<Connection>>();
   
       public Connection getConnection(String key) throws Exception {
           FutureTask<Connection> connectionTask = connectionPool.get(key);
           if (connectionTask != null) {
               return connectionTask.get();
           } else {
               Callable<Connection> callable = new Callable<Connection>() {
                   @Override
                   public Connection call() throws Exception {
                       return createConnection();
                   }
               };
               FutureTask<Connection> newTask = new FutureTask<Connection>(callable);
               connectionTask = connectionPool.putIfAbsent(key, newTask);
               if (connectionTask == null) {//进行添加
                   connectionTask = newTask;
                   connectionTask.run();
               }
               return connectionTask.get();
           }
       }
   
       //创建Connection
       private Connection createConnection() {
           return ConnectUtil.Connect();
       }
   
       public static void main(String[] args) throws Exception {
           NoLockSafeFuture noLockSafeFuture = new NoLockSafeFuture();
           Connection connection = noLockSafeFuture.getConnection("test");
           System.out.println(connection);
           System.out.println(noLockSafeFuture.connectionPool);
       }
   }
   
   ```

   这样的改造，可以避免由于高并发带来的多次创建连接及锁的出现。 