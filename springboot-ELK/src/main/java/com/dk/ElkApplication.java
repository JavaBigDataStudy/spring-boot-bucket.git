package com.dk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Description: ELK使用案例
 * @Author Cheri
 * @Date 2019/8/2 - 9:57
 * @Version V1.0
 **/
@SpringBootApplication
public class ElkApplication {

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(ElkApplication.class,args);
    }
}
