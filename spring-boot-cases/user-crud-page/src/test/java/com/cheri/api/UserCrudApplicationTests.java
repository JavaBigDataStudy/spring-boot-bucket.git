package com.cheri.api;

import com.cheri.api.bean.TUser;
import com.cheri.api.mapper.TUserMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

/**
 * @Description: 系统单元测试 [Junit + assert]
 * @ClassName UserCrudApplicationTests
 * @Author Cheri
 * @Date 2019/7/29 - 17:27
 * @Version V1.0
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserCrudApplicationTests {

    //注入需要的JavaBean
    @Autowired
    TUserMapper tUserMapper;

    //测试用户添加
    @Test
    public void testUserAdd(){
        TUser tUser = new TUser();
        tUser.setName("杜康");
        tUser.setAge(12);
        Date date = new Date();
        tUser.setCreatetime(date);
        tUser.setCreatorId("dukang");
        tUserMapper.insertSelective(tUser);
    }

    //测试用户修改
    @Test
    public void testUserUpdate(){

    }

    //测试查询指定用户
    @Test
    public void testUserQueryOne(){

    }

    //测试查询所有用户
    @Test
    public void testUserFindAll(){

    }

    //测试分页查询
    @Test
    public void testUserPageQuery(){

    }

    //测试用户删除
    @Test
    public void testUserDelete(){

    }
}
