package com.dk;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Description: 基于SpringBoot开发一个Restful服务实现增删改查功能
 * @Author Cheri
 * @Date 2019/8/4 - 16:31
 * @Version V1.0
 **/
@SpringBootApplication
@MapperScan("com.dk.dao")
public class SpringBootRestfulApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootRestfulApplication.class,args);
    }

}
