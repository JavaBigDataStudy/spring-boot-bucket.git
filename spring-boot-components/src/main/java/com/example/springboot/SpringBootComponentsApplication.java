package com.example.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Description:
 * @ClassName SpringBootComponentsApplication
 * @Author Cheri
 * @Date 2019/7/29 - 23:29
 * @Version V1.0
 **/
@SpringBootApplication
public class SpringBootComponentsApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootComponentsApplication.class,args);
    }
}
