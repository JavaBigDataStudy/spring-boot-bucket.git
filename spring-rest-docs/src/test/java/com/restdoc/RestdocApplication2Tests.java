package com.restdoc;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;

import static org.springframework.restdocs.headers.HeaderDocumentation.headerWithName;
import static org.springframework.restdocs.headers.HeaderDocumentation.requestHeaders;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RestdocApplication2Tests extends BaseRestDocTest{
	@Test
	public void contextLoads() throws Exception {
		this.mockMvc.perform(get("/hello?page=2&per_page=100").accept(MediaType.APPLICATION_JSON).header("Authorization", "Basic dXNlcjpzZWNyZXQ="))
				.andDo(print()).andExpect(status().isOk())
				.andDo(document("hello 接口",
						requestHeaders(
								headerWithName("Authorization").description(
										"Basic auth credentials")),
						requestParameters(
						parameterWithName("page").description("The page to retrieve"),
						parameterWithName("per_page").description("Entries per page")
				)));
	}
	@Test
	public void contextLoads2() throws Exception {
		this.mockMvc.perform(get("/hello2").accept(MediaType.APPLICATION_JSON))
				.andDo(print()).andExpect(status().isOk())
				.andDo(document("hello2 接口"));
	}


}
