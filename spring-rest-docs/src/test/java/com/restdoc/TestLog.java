package com.restdoc;

import org.apache.log4j.Logger;
import org.junit.Test;

/**
 * @Description: TODO
 * @Author Cheri
 * @Date 2019/8/2 - 2:46
 * @Version V1.0
 **/
public class TestLog {

    @Test
    public void testLoger(){

        final Logger logger = Logger.getLogger("TestErrOut");
        logger.debug(" This is debug!!!");
        logger.info(" This is info!!!");
        logger.warn(" This is warn!!!");
        logger.error(" This is error!!!");
        logger.fatal(" This is fatal!!!");
    }
}
