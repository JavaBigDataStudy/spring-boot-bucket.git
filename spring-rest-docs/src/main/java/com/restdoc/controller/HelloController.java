package com.restdoc.controller;

import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description: TODO
 * @ClassName OthersTests
 * @Author Cheri
 * @Date 2019/7/31 - 11:12
 * @Version V1.0
 **/
@RestController
public class HelloController {

//    @RequestMapping(value = "/hello",method = RequestMethod.GET)
    @GetMapping("/hello")
    public Map hello(@RequestParam("page") String page, @RequestParam("per_page") String perPage){
        Map<String, String> map = new HashMap<>();
        map.put("hello", "true");
        return map;
    }

    @RequestMapping(value = "/hello2",method = RequestMethod.GET)
    public Map hello(){
        Map<String, String> map = new HashMap<>();
        map.put("gogo", "true");
        return map;
    }

    @RequestMapping(value = "/hello3",method = RequestMethod.POST)
    public Map hello3(){
        Map<String, String> map = new HashMap<>();
        map.put("three", "true");
        return map;
    }
}
